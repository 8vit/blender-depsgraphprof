# Blender 2.8 Depsgraph Profiler Addon

![](https://ipfs.io/ipfs/Qma3NhgbSM4g6Q9jLvKm4ES7Fdn2v8ECERoe3XK97DtfPF/DepgraphProfiler20190422.jpg)

# Prerequisite

It has been tested with Blender 2.8 commit hash `a9481479776aa7c1998382a0cc52113fc4706c0c` (Apr 16, 2019). It might or might not work with a newer version because it relies on a not-stable-looking API.

# Installation

1. Download the repository [as a ZIP archive](https://gitlab.com/8vit/blender-depsgraphprof/-/archive/master/blender-depsgraphprof-master.zip).
2. User Preferences → Add-ons → Install...
3. Choose the ZIP archive downloaded in the first step.
4. Activate the check box next to "Development: Depsgraph Profiler".

# License

This project is licensed under [GNU General Public License v3.0](https://www.gnu.org/licenses/gpl-3.0.en.html) or later.
