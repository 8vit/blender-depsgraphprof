import bpy
from .acquisition import get_stats_simple


_INSTRUCTIONS = """
Instructions:
1. Check 'Enable Capture'
2. Trigger Depsgraph evaluation,
  e.g., by moving objects around
3. Click 'Refresh'
"""

_ICON_MAP = {
    'OB': 'OBJECT_DATA',
    'ME': 'MESH_DATA',
    'NT': 'NODETREE',
    'MA': 'MATERIAL_DATA',
    'KE': 'SHAPEKEY_DATA',
    'AR': 'ARMATURE_DATA',
    'PA': 'PARTICLE_DATA',
    'CU': 'CURVE_DATA',
    'GR': 'GROUP',
    'SC': 'SCENE',
    'MB': 'META_DATA',
    'LT': 'LATTICE_DATA',
    'LA': 'LIGHT_DATA',
    'LP': 'OUTLINER_OB_LIGHTPROBE',
    'CA': 'CAMERA_DATA',
    'SK': 'SPEAKER',
    'WO': 'WORLD_DATA',
}


class VIEW3D_PT_depsgraph_profiler(bpy.types.Panel):
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Profiler"
    bl_label = "Depsgraph"

    def draw(self, context: bpy.types.Context):
        layout = self.layout

        state = context.window_manager.depsgraph_profiler_state

        layout.prop(state, 'debug_depsgraph_time')
        layout.operator('depsgraphprof.read_profile',
                        text="Refresh",
                        icon='FILE_REFRESH')

        layout.separator()

        if len(state.rows) == 0:
            for line in _INSTRUCTIONS.split('\n'):
                layout.label(text=line)
        else:
            row = layout.row()
            cell = row.column()
            cell.label(text="Name")
            cell = row.column()
            cell.alignment = 'RIGHT'
            cell.label(text="[ms]", translate=False)

            for state_row in state.rows:
                row = layout.row()
                row.enabled = False  # Disable editing

                name = state_row.name
                icon = _ICON_MAP.get(name[1:3])
                if icon:
                    name = name[5:]
                else:
                    icon = 'QUESTION'

                row.label(text='', icon=icon)

                row.prop(state_row, 'duration',
                         text=name,
                         translate=False,
                         icon=icon,
                         slider=True)


class DepsgraphProfilerUIStateRow(bpy.types.PropertyGroup):
    name: bpy.props.StringProperty()
    duration: bpy.props.FloatProperty(
        soft_min=0, soft_max=200, precision=3, options=set())


class DepsgraphProfilerUIState(bpy.types.PropertyGroup):
    def get_debug_depsgraph_time(self):
        return bpy.app.debug_depsgraph_time

    def set_debug_depsgraph_time(self, value: bool):
        bpy.app.debug_depsgraph_time = value

    debug_depsgraph_time: bpy.props.BoolProperty(
        name="Enable Capture",
        description="Collect timing information when evaluating a depsgraph",
        get=get_debug_depsgraph_time,
        set=set_debug_depsgraph_time,
    )

    rows: bpy.props.CollectionProperty(type=DepsgraphProfilerUIStateRow)


class ReadDepsgraphProfile(bpy.types.Operator):
    bl_label = "Read Depsgraph Profile"
    bl_description = "Read Depsgraph Profile to UI"
    bl_idname = 'depsgraphprof.read_profile'
    bl_options = {'INTERNAL'}

    def execute(self, context: bpy.types.Context):
        state = context.window_manager.depsgraph_profiler_state

        # Acquire the stats
        stats = get_stats_simple(context)

        # Update the UI state
        state.rows.clear()
        for item in stats:
            row = state.rows.add()
            row.name = item.name
            row.duration = item.duration * 1000  # s -> ms

        return {'FINISHED'}


def register():
    bpy.utils.register_class(DepsgraphProfilerUIStateRow)
    bpy.utils.register_class(DepsgraphProfilerUIState)
    bpy.types.WindowManager.depsgraph_profiler_state = \
        bpy.props.PointerProperty(type=DepsgraphProfilerUIState)
    bpy.utils.register_class(VIEW3D_PT_depsgraph_profiler)
    bpy.utils.register_class(ReadDepsgraphProfile)


def unregister():
    bpy.utils.unregister_class(ReadDepsgraphProfile)
    bpy.utils.unregister_class(VIEW3D_PT_depsgraph_profiler)
    del bpy.types.WindowManager.depsgraph_profiler_state
    bpy.utils.unregister_class(DepsgraphProfilerUIState)
    bpy.utils.unregister_class(DepsgraphProfilerUIStateRow)


if __name__ == '__main__':
    register()
