import bpy
import os
from typing import List, NamedTuple
from tempfile import mkstemp


class SimpleStatsItem(NamedTuple):
    name: str
    duration: float


def get_stats_simple(context: bpy.types.Context) -> List[SimpleStatsItem]:
    f, temp_filename = mkstemp()
    os.close(f)
    try:
        # Generate a Gnuplot script
        context.depsgraph.debug_stats_gnuplot(temp_filename, '')

        # Extract the result from the script
        with open(temp_filename) as f:
            gp_script = f.read()
    finally:
        os.unlink(temp_filename)

    lines = gp_script.split('\n')
    items = []
    for line in lines:
        if line == 'EOD':
            break
        i = line.rfind(',')
        if i >= 0:
            name = line[0:i]

            if name[0] == '"':
                name = name[1:-1]
                name = name.replace('\\\\\\', '')

            items.append(SimpleStatsItem(name, float(line[i + 1:])))

    items.reverse()

    return items
