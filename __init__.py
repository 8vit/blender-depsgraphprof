import bpy
from . import panel


bl_info = {
    "name": "Depsgraph Profiler",
    "author": "Yavit",
    "version": (0, 1, 0),
    "blender": (2, 80, 0),
    "location": "Viewport UI",
    "description": "A user-friendly front-end to Depsgraph's time profiling system",
    "warning": "",
    "tracker_url": "",
    "wiki_url": "",
    "category": "Development"
}


def register():
    panel.register()


def unregister():
    panel.unregister()
